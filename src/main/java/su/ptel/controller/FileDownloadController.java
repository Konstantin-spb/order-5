package su.ptel.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

@Controller
@RequestMapping("/files")
public class FileDownloadController {

    public final static String FILE_PATH = "/opt/Files/order-5/";

    @RequestMapping(value = "/{fileName:.+}", method = RequestMethod.GET)
    public
    @ResponseBody
    void downloadFiles(HttpServletRequest request,
                       HttpServletResponse response,
                       @PathVariable String fileName) {

        ServletContext context = request.getServletContext();

        File downloadFile = new File(FILE_PATH + fileName);
        FileInputStream inputStream = null;
        OutputStream outStream = null;

        if(!downloadFile.exists()){
            String errorMessage = "Sorry. The file you are looking for does not exist";
            System.out.println(errorMessage);
            OutputStream outputStream = null;
            try {
                outputStream = response.getOutputStream();
                outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return;
        }

        try {
            inputStream = new FileInputStream(downloadFile);

            response.setContentLength((int) downloadFile.length());
            response.setContentType(context.getMimeType(FILE_PATH + fileName));

            // response header
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // Write response
            outStream = response.getOutputStream();
            IOUtils.copy(inputStream, outStream);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != inputStream)
                    inputStream.close();
                if (null != inputStream)
                    outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}