package su.ptel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import su.ptel.dao.ContragentsDao;
import su.ptel.dao.FileDao;
import su.ptel.model.Contragent;
import su.ptel.model.File;
import su.ptel.model.OrderDataModel;
import su.ptel.service.FileService;
import su.ptel.service.OrderDataService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by konstantin on 10.06.16.
 */
@Controller
@RequestMapping("/orders")
public class OrdersController {

//    @Autowired
//    AllOrderInfoDao allOrderInfoDao;

    @Autowired
    OrderDataService dataService;

    @Autowired
    ContragentsDao contragentsDao;

    @Autowired
    FileDao fileDao;

    @Autowired
    FileService fileService;

    @RequestMapping(method = RequestMethod.GET, params = "new")
    public String createOrder(Model model) {
        model.addAttribute(new OrderDataModel());
        List<Contragent> contragentList = contragentsDao.getList();

        model.addAttribute("contragentsList", contragentList);
        return "orders/edit-n";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, params = "edit")
    public String editOrder(@PathVariable int id, Model model) {
        model.addAttribute("contragentsList", contragentsDao.getList());
        model.addAttribute("firmList", fileDao.getList());
        model.addAttribute(dataService.getOrderById(id));
        return "orders/edit-n";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, params = "delete")
    public String deleteOrder(@PathVariable int id) {
        dataService.deleteOrder(id);
        return "redirect:/orders/orders";
    }

    @RequestMapping(value = {"", "/{id}"}, method = RequestMethod.POST)
    public String addOrderFromForm (@Valid  OrderDataModel orderDataModel,
                                   BindingResult bindingResult,
                                   Model model,
                                   @RequestParam("importFile") MultipartFile file) {
        if (bindingResult.hasErrors()) {
//            model.addAttribute(new OrderDataModel());
            model.addAttribute("contragentsList", contragentsDao.getList());
            return "orders/edit-n";
        }

        Contragent newContragent;
        if (orderDataModel.getContragentModel().getId() != -1) {
            int i = orderDataModel.getContragentModel().getId();
            newContragent = contragentsDao.getRowById(i);
        } else {
            newContragent = new Contragent();
            newContragent.setFirmName(orderDataModel.getContragentModel().getFirmName());
            contragentsDao.saveOrUpdate(newContragent);
        }
        orderDataModel.setContragentModel(newContragent);
        dataService.createNewOrUpdateOrder(orderDataModel);

        if (file != null && !file.getOriginalFilename().isEmpty()) {
            System.out.println(file.getOriginalFilename() + "---" + file.getName());
            fileService.saveFile(file.getOriginalFilename(), file, orderDataModel);
        }
        newContragent = null;
        orderDataModel = null;
        return "redirect:/orders/orders";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String getOrdersList(Model model) {
        model.addAttribute("orders", dataService.getOrderList());
        return "/orders/orders";
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {

        Contragent contragent = new Contragent();
        OrderDataModel orderDataModel = new OrderDataModel();

        contragent.setFirmName("New Contragent-132");
        contragent.setFirmAddress("New Address");
        contragentsDao.saveOrUpdate(contragent);

        orderDataModel.setOrderNumber("1234567890");
        orderDataModel.setWorksname("asdfghjkl");
        orderDataModel.setContragentModel(contragent);
        int tempId = dataService.createNewOrUpdateOrder(orderDataModel);

        for (int i = 0; i < 10; i++) {
            File file = new File();
            file.setOrderData(orderDataModel);

            file.setFilePath("/path_to_file" + "-" + i);
            fileDao.saveOrUpdate(file);

        }

        for(File f: dataService.getOrderById(tempId).getFiles()) {
            System.out.println(new StringBuilder()
                    .append(f.getId()).append(" --- ")
                    .append(f.getFilePath()).append(" --- ")
                    .append(f.getOrderData().getContragentModel().getFirmName()));
        }
        return "redirect:/orders/orders";
    }
}
