package su.ptel.model;


import javax.persistence.*;

/**
 * Created by konstantin on 18.04.16.
 */

//@Repository
@Entity
@Table(name = "CONTRAGENT")
//@Scope("prototype")
public class Contragent {

    public Contragent() {
    }

    public Contragent(String firmName) {
        this.firmName = firmName;
    }

    public Contragent(String firmName, String firmAddress) {
        this.firmName = firmName;
        this.firmAddress = firmAddress;
    }

    //    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contragent_id")
    private int id;

    @Column(name = "firm_name")
    private String firmName;

    @Column(name = "firm_address")
    private String firmAddress;

    @Column(name = "INN")
    private String inn;

    @Column(name = "TELEPHON")
    private String telephon;

    @Column(name = "USER_FOR_CONTACT")
    private String userForContact;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getFirmAddress() {
        return firmAddress;
    }

    public void setFirmAddress(String firmAddress) {
        this.firmAddress = firmAddress;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getTelephon() {
        return telephon;
    }

    public void setTelephon(String telephon) {
        this.telephon = telephon;
    }

    public String getUserForContact() {
        return userForContact;
    }

    public void setUserForContact(String userForContact) {
        this.userForContact = userForContact;
    }
}
