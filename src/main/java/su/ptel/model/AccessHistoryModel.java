package su.ptel.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by konstantin on 18.04.16.
 */

//@Repository
@Entity
@Table(name = "ACCESS_HISTORY")
public class AccessHistoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "USER_ID")
    private String firmName;

    @Column(name = "ACCESS_DATE")
    private Date firmAdress;

    public OrderDataModel getOrderDataModel() {
        return orderDataModel;
    }

    public void setOrderDataModel(OrderDataModel orderDataModel) {
        this.orderDataModel = orderDataModel;
    }

    @ManyToOne
    @JoinColumn(name="ORDER_ID")
    private OrderDataModel orderDataModel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public Date getFirmAdress() {
        return firmAdress;
    }

    public void setFirmAdress(Date firmAdress) {
        this.firmAdress = firmAdress;
    }
}
