package su.ptel.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ORDER_DATA")
public class OrderDataModel {

    public OrderDataModel() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id",  nullable = false)
    private int id;

    @Column(name = "order_number")
//    @Size(min = 3, max = 6, message = "Invalide order number")
    private String orderNumber;

//    @Pattern(regexp = "^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$", message = "Invalide date formate")
    @Column(name = "ORDER_DATE")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @NotNull
    private Date orderDate;//Дата ордера

    @Lob
    @Column(name = "worksname")
    private String worksname;

    @Column(name = "sum_pir")
    private int sumPIR;

    @Column(name = "sum_smr")
    private int sumSMR;

//    @NotNull
    @Column(name = "turnaround_time_start")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date turnaroundTimeStart; //сроки выполнения  работ

    @Column(name = "turnaround_time_end")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date turnaroundTimeEnd; //сроки выполнения  работ

    @Column(name = "prepayment")
    private String prepayment; //Оплата(аванс)

    @Column(name = "record_of_acceptance_pir_number")
    private String recordOfAcceptancePirNumber; //акт сдачи-приёмки ПИР

    @Column(name = "record_of_acceptance_pir_date")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date recordOfAcceptancePirDate; //акт сдачи-приёмки ПИР дата

    @Column(name = "current_state")
    private String currentState;

    @Column(name = "record_of_acceptance_smr_number")
    private String recordOfAcceptanceSmrNumber; //акт сдачи-приёмки СМР

    @Column(name = "record_of_acceptance_smr_date")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date recordOfAcceptanceSmrDate; //акт сдачи-приёмки СМР

    @Column(name = "sum_record_of_acceptance_pir_with_nds")
    private int sumRecordOfAcceptancePirWithNds; //Сумма Акт сдачи-приемкиПИР (с НДС)

    @Column(name = "sum_record_of_acceptance_smr_with_nds")
    private int sumRecordOfAcceptanceSmrWithNds; //Сумма Акт сдачи-приемкиСМР (с НДС)

    @Column(name = "notes_estimate_and_contract_department")
    private String notesEstimateAndContractDepartment; //Примечания сметно-договорного отдела

    @Column(name = "project_agreed")
    private String projectAgreed; //Проект (согласован )

    @Column(name = "approval_project_with_services")
    private String approvalProjectWithServices; //Согласование проекта (со службами)

    @Column(name = "developer")
    private String developer;

    @Column(name = "actual_performance_of_work_percentage_of_total")
    private int actualPerformanceOfWorkPercentageOfTotal; //Фактическое выполнение работ( в %, от общего объема )

    @Column(name = "producer_of_work")
    private  String producerOfWork; //Производитель работ

    @Column(name = "executive_documentation_electronic")
    private String executiveDocumentationElectronic; //Исполнительная документация (электронный вид)

     @Column(name = "executive_documentation_with_sign")
    private String executiveDocumentationWithSign; //Исполнительная документация (подписанная)

    @Column(name = "development_executive_documents")
    private String developmentExecutiveDocuments; //Разработчик Исполнительной документации

    @Column(name = "invoices_copies_of_invoices")
    private String invoicesCopiesOfInvoices;

    @Column(name = "costs_of_the_whole_project")
    private String costsOfTheWholeProject; //Затраты (по объекту в целом)

    @Column(name = "handover_of_documentation")
    private String handoverOfDocumentation;

    @Column(name = "list_of_documents_after_end_job")
    private String listOfDocumentsAfterEndJob;

    @Column(name = "active_or_not")
    private String activeOrNot;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "contragent_id")
//    @NotNull
    private Contragent contragentModel;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "orderData", cascade = CascadeType.ALL)//Вписываем имя поня из класса File
    Set<File> files = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getWorksname() {
        return worksname;
    }

    public void setWorksname(String worksname) {
        this.worksname = worksname;
    }

    public int getSumPIR() {
        return sumPIR;
    }

    public void setSumPIR(int sumPIR) {
        this.sumPIR = sumPIR;
    }

    public int getSumSMR() {
        return sumSMR;
    }

    public void setSumSMR(int sumSMR) {
        this.sumSMR = sumSMR;
    }

    public Date getTurnaroundTimeStart() {
        return turnaroundTimeStart;
    }

    public void setTurnaroundTimeStart(Date turnaroundTimeStart) {
        this.turnaroundTimeStart = turnaroundTimeStart;
    }

    public Date getTurnaroundTimeEnd() {
        return turnaroundTimeEnd;
    }

    public void setTurnaroundTimeEnd(Date turnaroundTimeEnd) {
        this.turnaroundTimeEnd = turnaroundTimeEnd;
    }

    public String getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(String prepayment) {
        this.prepayment = prepayment;
    }

    public String getRecordOfAcceptancePirNumber() {
        return recordOfAcceptancePirNumber;
    }

    public void setRecordOfAcceptancePirNumber(String recordOfAcceptancePirNumber) {
        this.recordOfAcceptancePirNumber = recordOfAcceptancePirNumber;
    }

    public Date getRecordOfAcceptancePirDate() {
        return recordOfAcceptancePirDate;
    }

    public void setRecordOfAcceptancePirDate(Date recordOfAcceptancePirDate) {
        this.recordOfAcceptancePirDate = recordOfAcceptancePirDate;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getRecordOfAcceptanceSmrNumber() {
        return recordOfAcceptanceSmrNumber;
    }

    public void setRecordOfAcceptanceSmrNumber(String recordOfAcceptanceSmrNumber) {
        this.recordOfAcceptanceSmrNumber = recordOfAcceptanceSmrNumber;
    }

    public Date getRecordOfAcceptanceSmrDate() {
        return recordOfAcceptanceSmrDate;
    }

    public void setRecordOfAcceptanceSmrDate(Date recordOfAcceptanceSmrDate) {
        this.recordOfAcceptanceSmrDate = recordOfAcceptanceSmrDate;
    }

    public int getSumRecordOfAcceptancePirWithNds() {
        return sumRecordOfAcceptancePirWithNds;
    }

    public void setSumRecordOfAcceptancePirWithNds(int sumRecordOfAcceptancePirWithNds) {
        this.sumRecordOfAcceptancePirWithNds = sumRecordOfAcceptancePirWithNds;
    }

    public int getSumRecordOfAcceptanceSmrWithNds() {
        return sumRecordOfAcceptanceSmrWithNds;
    }

    public void setSumRecordOfAcceptanceSmrWithNds(int sumRecordOfAcceptanceSmrWithNds) {
        this.sumRecordOfAcceptanceSmrWithNds = sumRecordOfAcceptanceSmrWithNds;
    }

    public String getNotesEstimateAndContractDepartment() {
        return notesEstimateAndContractDepartment;
    }

    public void setNotesEstimateAndContractDepartment(String notesEstimateAndContractDepartment) {
        this.notesEstimateAndContractDepartment = notesEstimateAndContractDepartment;
    }

    public String getProjectAgreed() {
        return projectAgreed;
    }

    public void setProjectAgreed(String projectAgreed) {
        this.projectAgreed = projectAgreed;
    }

    public String getApprovalProjectWithServices() {
        return approvalProjectWithServices;
    }

    public void setApprovalProjectWithServices(String approvalProjectWithServices) {
        this.approvalProjectWithServices = approvalProjectWithServices;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public int getActualPerformanceOfWorkPercentageOfTotal() {
        return actualPerformanceOfWorkPercentageOfTotal;
    }

    public void setActualPerformanceOfWorkPercentageOfTotal(int actualPerformanceOfWorkPercentageOfTotal) {
        this.actualPerformanceOfWorkPercentageOfTotal = actualPerformanceOfWorkPercentageOfTotal;
    }

    public String getProducerOfWork() {
        return producerOfWork;
    }

    public void setProducerOfWork(String producerOfWork) {
        this.producerOfWork = producerOfWork;
    }

    public String getExecutiveDocumentationElectronic() {
        return executiveDocumentationElectronic;
    }

    public void setExecutiveDocumentationElectronic(String executiveDocumentationElectronic) {
        this.executiveDocumentationElectronic = executiveDocumentationElectronic;
    }

    public String getExecutiveDocumentationWithSign() {
        return executiveDocumentationWithSign;
    }

    public void setExecutiveDocumentationWithSign(String executiveDocumentationWithSign) {
        this.executiveDocumentationWithSign = executiveDocumentationWithSign;
    }

    public String getDevelopmentExecutiveDocuments() {
        return developmentExecutiveDocuments;
    }

    public void setDevelopmentExecutiveDocuments(String developmentExecutiveDocuments) {
        this.developmentExecutiveDocuments = developmentExecutiveDocuments;
    }

    public String getInvoicesCopiesOfInvoices() {
        return invoicesCopiesOfInvoices;
    }

    public void setInvoicesCopiesOfInvoices(String invoicesCopiesOfInvoices) {
        this.invoicesCopiesOfInvoices = invoicesCopiesOfInvoices;
    }

    public String getCostsOfTheWholeProject() {
        return costsOfTheWholeProject;
    }

    public void setCostsOfTheWholeProject(String costsOfTheWholeProject) {
        this.costsOfTheWholeProject = costsOfTheWholeProject;
    }

    public String getHandoverOfDocumentation() {
        return handoverOfDocumentation;
    }

    public void setHandoverOfDocumentation(String handoverOfDocumentation) {
        this.handoverOfDocumentation = handoverOfDocumentation;
    }

    public String getListOfDocumentsAfterEndJob() {
        return listOfDocumentsAfterEndJob;
    }

    public void setListOfDocumentsAfterEndJob(String listOfDocumentsAfterEndJob) {
        this.listOfDocumentsAfterEndJob = listOfDocumentsAfterEndJob;
    }

    public String getActiveOrNot() {
        return activeOrNot;
    }

    public void setActiveOrNot(String activeOrNot) {
        this.activeOrNot = activeOrNot;
    }

    public Contragent getContragentModel() {
        return contragentModel;
    }

    public void setContragentModel(Contragent contragentModel) {
        this.contragentModel = contragentModel;
    }

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }
}
