package su.ptel.dao;


import su.ptel.model.OrderDataModel;
import java.util.List;

/**
 * Created by konstantin on 16.03.16.
 */
public interface OrderDataDao {

    List<OrderDataModel> getList();
    OrderDataModel getRowById(int id);
    int saveOrUpdate(OrderDataModel orderDataModel);
    int delete(int id);
}
