package su.ptel.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import su.ptel.dao.OrderDataDao;
import su.ptel.model.OrderDataModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by konstantin on 16.03.16.
 */

@Component
public class OrderDataImpl implements OrderDataDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<OrderDataModel> getList() {
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<OrderDataModel> orderDataModelList = session.createQuery("from OrderDataModel")
                .list();
        session.close();
        return orderDataModelList;
    }

    @Override
    @Transactional
    public OrderDataModel getRowById(int id) {
        Session session = sessionFactory.openSession();
        OrderDataModel orderDataModel = (OrderDataModel) session.get(OrderDataModel.class, id);
        session.close();
        return orderDataModel;
    }

    @Override
    @Transactional
    public int saveOrUpdate(OrderDataModel orderDataModel) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(orderDataModel);
        tx.commit();
        Serializable id = session.getIdentifier(orderDataModel);
        session.close();
        return (Integer) id;
    }

    @Override
    @Transactional
    public int delete(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        OrderDataModel orderDataModel = (OrderDataModel) session.load(OrderDataModel.class, id);
        session.delete(orderDataModel);
        tx.commit();
        Serializable ids = session.getIdentifier(orderDataModel);
        session.close();
        return (Integer) ids;
    }
}
