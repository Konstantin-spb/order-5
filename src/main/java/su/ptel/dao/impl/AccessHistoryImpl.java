package su.ptel.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import su.ptel.dao.AccessHistoryDao;
import su.ptel.model.AccessHistoryModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by konstantin on 02.06.16.
 */

@Component
public class AccessHistoryImpl implements AccessHistoryDao{

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<AccessHistoryModel> getList() {
        Session session = sessionFactory.openSession();
//        @SuppressWarnings("unchecked")
        List<AccessHistoryModel> accessHistoryModels = session.createQuery("from AccessHistoryModel")
                .list();
        session.close();
        return accessHistoryModels;
    }

    @Override
    @Transactional
    public int saveOrUpdate(AccessHistoryModel accessHistoryModel) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(accessHistoryModel);
        tx.commit();
        Serializable id = session.getIdentifier(accessHistoryModel);
        session.close();
        return (Integer) id;
    }

    @Override
    @Transactional
    public int delete(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        AccessHistoryModel accessHistoryModel = (AccessHistoryModel) session.load(AccessHistoryModel.class, id);
        session.delete(accessHistoryModel);
        tx.commit();
        Serializable ids = session.getIdentifier(accessHistoryModel);
        session.close();
        return (Integer) ids;
    }

    @Override
    @Transactional
    public AccessHistoryModel getRowById(int id) {
        Session session = sessionFactory.openSession();
        AccessHistoryModel accessHistoryModel = (AccessHistoryModel) session.load(AccessHistoryModel.class, id);
        return accessHistoryModel;
    }
    
}
