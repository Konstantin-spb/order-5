package su.ptel.dao.impl;

import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import su.ptel.dao.AbstractDao;
import su.ptel.dao.ContragentsDao;
import su.ptel.model.Contragent;
import su.ptel.model.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by konstantin on 03.06.16.
 */
@Component
public class ContragentImpl extends AbstractDao<Integer, Contragent> implements ContragentsDao{

//        @Autowired
//        SessionFactory sessionFactory;
//        Session session;

//    public ContragentImpl(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }

//        @Override
//        @Transactional
//        public List<Contragent> getList() {
//            try {
//                session = sessionFactory.getCurrentSession();
//            } catch (HibernateException e) {
//                session = sessionFactory.openSession();
//            }
//            List<Contragent> contragents = session.createQuery("from Contragent")
//                    .list();
//            session.close();
//            return contragents;
//        }

    public List<Contragent> getList() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("firmName"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
        List<Contragent> contragent = (List<Contragent>) criteria.list();

        // No need to fetch userProfiles since we are not showing them on list page. Let them lazy load.
        // Uncomment below lines for eagerly fetching of userProfiles if you want.
		/*
		for(User user : users){
			Hibernate.initialize(user.getUserProfiles());
		}*/
        return contragent;
    }

    @Override
    public Contragent getRowById(int id) {
        Contragent contragent = getByKey(id);
        return contragent;
    }

    @Override
    public int saveOrUpdate(Contragent contragent) {
        return 0;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

//        @Override
//        @Transactional
//        public int saveOrUpdate(Contragent contragent) {
//            try {
//                session = sessionFactory.getCurrentSession();
//            } catch (HibernateException e) {
//                session = sessionFactory.openSession();
//            }
//            Transaction tx = session.beginTransaction();
//            session.saveOrUpdate(contragent);
//            tx.commit();
//            Serializable id = session.getIdentifier(contragent);
//            session.close();
//            return (Integer) id;
//        }
//
//        @Override
//        @Transactional
//        public int delete(int id) {
//            try {
//                session = sessionFactory.getCurrentSession();
//            } catch (HibernateException e) {
//                session = sessionFactory.openSession();
//            }
//            Transaction tx = session.beginTransaction();
//            Contragent contragent = (Contragent) session.load(Contragent.class, id);
//            session.delete(contragent);
//            tx.commit();
//            Serializable ids = session.getIdentifier(contragent);
//            session.close();
//            return (Integer) ids;
//        }
//
//        @Override
//        @Transactional
//        public Contragent getRowById(int id) {
//            try {
//                session = sessionFactory.getCurrentSession();
//            } catch (HibernateException e) {
//                session = sessionFactory.openSession();
//            }
//            Contragent contragent = (Contragent) session.load(Contragent.class, id);
//            session.close();
//
//            return contragent;
//        }

}
