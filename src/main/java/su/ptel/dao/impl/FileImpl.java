package su.ptel.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import su.ptel.dao.FileDao;
import su.ptel.model.File;

import java.io.Serializable;
import java.util.List;

/**
 * Created by konstantin on 23.06.16.
 */

@Repository
public class FileImpl implements FileDao{

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<File> getList() {
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<File> files = session.createQuery("from File")
                .list();
        session.close();
        return files;
    }

    @Override
    @Transactional
    public File getRowById(int id) {
        Session session = sessionFactory.openSession();
        File orderDataModel = (File) session.get(File.class, id);
        session.close();
        return orderDataModel;
    }

    @Override
    @Transactional
    public int saveOrUpdate(File file) {
        Session session = sessionFactory.openSession();
        session.saveOrUpdate(file);
        Serializable id = session.getIdentifier(file);
        session.close();
        return (Integer) id;
    }

    @Override
    @Transactional
    public int delete(int id) {
        Session session = sessionFactory.openSession();
        File file = (File) session.load(File.class, id);
        session.delete(file);
        Serializable ids = session.getIdentifier(file);
        session.close();
        return (Integer) ids;
    }

    @Override
    public List<File> getFilesFromOrder(int id) {
        Session session = sessionFactory.openSession();

        List<File> files =
                session.createCriteria(File.class)
                        .add(Restrictions.eq("id", id))
                        .list();
        return files;
    }
}
