package su.ptel.dao;

import su.ptel.model.AccessHistoryModel;

import java.util.List;

/**
 * Created by konstantin on 02.06.16.
 */
public interface AccessHistoryDao {

    List<AccessHistoryModel> getList();

    AccessHistoryModel getRowById(int id);

    int saveOrUpdate(AccessHistoryModel accessHistoryModel);

    int delete(int id);

}
