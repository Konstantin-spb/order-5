package su.ptel.dao;


import su.ptel.model.Contragent;

import java.util.List;

/**
 * Created by konstantin on 24.05.16.
 */
public interface ContragentsDao {

    List<Contragent> getList();
    Contragent getRowById(int id);
    int saveOrUpdate(Contragent contragent);
    int delete(int id);

}
