package su.ptel.dao;

import su.ptel.model.File;

import java.util.List;

/**
 * Created by konstantin on 23.06.16.
 */
public interface FileDao {

    List<File> getList();
    File getRowById(int id);
    int saveOrUpdate(File file);
    int delete(int id);
    List<File> getFilesFromOrder(int id);
}
