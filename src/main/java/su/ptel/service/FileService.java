package su.ptel.service;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import su.ptel.controller.FileDownloadController;
import su.ptel.dao.FileDao;
import su.ptel.model.OrderDataModel;

import java.io.File;
import java.io.IOException;

@Component
@Scope("prototype")
public class FileService {

    @Autowired
    FileDao fileDao;

//    public static final String FILE_PATH = "/home/konstantin/IdeaProjects/order-4/src/main/resources/";
    @Transactional
    public void saveFile(String filename, MultipartFile importFile, OrderDataModel orderDataModel) {

        su.ptel.model.File orderFile = new su.ptel.model.File();
        orderFile.setFilePath(FileDownloadController.FILE_PATH);
        orderFile.setFileName(filename);
        orderFile.setOrderData(orderDataModel);
        fileDao.saveOrUpdate(orderFile);
        try {
            File file = new File(FileDownloadController.FILE_PATH + filename);
            FileUtils.writeByteArrayToFile(file, importFile.getBytes());
        } catch (IOException e) {
//            throw new ImageUploadException("Unable to save image", e);
            e.printStackTrace();
        }
    }
}
