package su.ptel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import su.ptel.dao.OrderDataDao;
import su.ptel.model.OrderDataModel;

import java.util.List;

/**
 * Created by konstantin on 15.06.16.
 */

@Component
public class OrderDataService {

    @Autowired
    OrderDataDao orderDataDao;

    public int createNewOrUpdateOrder(OrderDataModel orderDataModel) {
        return orderDataDao.saveOrUpdate(orderDataModel);
    }

    public OrderDataModel getOrderById( int id) {
        return orderDataDao.getRowById(id);
    }

    public void deleteOrderById(int id) {
        orderDataDao.delete(id);
    }

    public List<OrderDataModel> getOrderList() {
        return orderDataDao.getList();
    }

    public void deleteOrder(int id) {
        orderDataDao.delete(id);
    }

}
