package su.ptel.service;

import su.ptel.model.UserProfile;
import java.util.List;

public interface UserProfileService {

	UserProfile findById(int id);

	UserProfile findByType(String type);
	
	List<UserProfile> findAll();
	
}
