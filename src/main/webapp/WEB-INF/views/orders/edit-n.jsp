<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>User Registration Form</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UT">
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<div class="generic-container">
    <%@include file="../authheader.jsp" %>

    <div class="well lead">New Order Registration Form</div>
    <sf:form method="post" modelAttribute="orderDataModel" enctype="multipart/form-data">
        <sf:input type="hidden" path="id" id="id"/>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" >Выберите файл</label>
                <div class="col-md-7">
                    <input name="importFile" type="file"<%-- multiple--%> />
                </div>
                <div class="col-lg-7"></div>
                <div class="col-lg-7">
                    <c:choose><%--Это конструкция if...else--%>
                        <c:when test="${orderDataModel.files.size() > 0}">
                            <c:forEach items="${orderDataModel.files}" var="file">
                                <a href="/files/${file.fileName}" download>${file.fileName}</a><br>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            Нет прикреплённых файлов
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="id_order_number">№ договора</label>
                <div class="col-md-7">
                    <sf:input path="orderNumber" id="id_order_number" class="form-control input-sm"/>
                    <div class="has-error">
                        <sf:errors path="orderNumber" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="orderDate">Дата договора</label>
                <div class="col-md-7">
                    <sf:input type="text" path="orderDate" placeholder="dd-MM-yyyy" id="orderDate"
                              class="form-control input-sm"/>
                    <div class="has-error">
                        <sf:errors path="orderDate" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="contragent">Контрагент</label>
                <div class="col-md-7">
                    <c:if test="${orderDataModel.contragentModel.firmName.length() > 0}">
                        ${orderDataModel.contragentModel.firmName = ""}
                        ${orderDataModel.contragentModel.firmAddress = ""}
                    </c:if>
                    <sf:input path="contragentModel.firmName" id="contragent" class="form-control input-sm"/> - Firm
                    name
                    <div class="has-error">
                        <sf:errors path="contragentModel.firmName" class="help-inline"/>
                    </div>
                    <br>
                    <sf:input path="contragentModel.firmAddress" id="contragent" class="form-control input-sm"/> - Firm
                    address
                    <div class="has-error">
                        <sf:errors path="contragentModel.firmAddress" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="listFirName">или выберите фирму из списка</label>
                <div class="col-md-7">
                    <sf:select path="contragentModel.id" id="listFirName" class="form-control input-sm">
                        <sf:option value="-1" label="---Выберите фирму"/>
                        <sf:options items="${contragentsList}" itemValue="id" itemLabel="firmName"/>
                    </sf:select>
                    <div class="has-error">
                        <sf:errors path="contragentModel.id" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="worksname">Наименование работ</label>
                <div class="col-md-7">
                    <sf:input path="worksname" id="worksname" class="form-control input-sm"/>
                    <div class="has-error">
                        <sf:errors path="worksname" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="currentState">Текущее состояние</label>
                <div class="col-md-7">
                    <sf:input type="text" path="currentState" id="currentState" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="currentState" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="id_sumPIR">Сумма ПИР</label>
                <div class="col-md-7">
                    <sf:input path="sumPIR" id="id_sumPIR" class="form-control input-sm"/>
                    <div class="has-error">
                        <sf:errors path="sumPIR" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="id_sumSMR">Сумма СМР</label>
                <div class="col-md-7">
                    <sf:input path="sumSMR" id="id_sumSMR" class="form-control input-sm"/>
                    <div class="has-error">
                        <sf:errors path="sumSMR"  class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable">Сроки выполнения работ</label>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="id_turnaroundTimeStart">начало</label>
                <div class="col-md-7">
                    <sf:input path="turnaroundTimeStart" id="id_turnaroundTimeStart" placeholder="dd-MM-yyyy" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="turnaroundTimeStart" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="id_turnaroundTimeEnd">окончание</label>
                <div class="col-md-7">
                    <sf:input path="turnaroundTimeEnd" id="id_turnaroundTimeEnd" placeholder="dd-MM-yyyy" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="turnaroundTimeEnd" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="prepayment">Оплата(ованс)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="prepayment" id="prepayment" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="prepayment" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable">Акт сдачи-приемки ПИР</label>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="recordOfAcceptancePirNumber">номер</label>
                <div class="col-md-7">
                    <sf:input type="text" path="recordOfAcceptancePirNumber" id="recordOfAcceptancePirNumber" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="recordOfAcceptancePirNumber" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="recordOfAcceptancePirDate">Дата</label>
                <div class="col-md-7">
                    <sf:input type="text" placeholder="dd-MM-yyyy" path="recordOfAcceptancePirDate" id="recordOfAcceptancePirDate" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="recordOfAcceptancePirDate" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable">Акт сдачи-приемки СМР</label>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="recordOfAcceptanceSmrNumber">номер</label>
                <div class="col-md-7">
                    <sf:input type="text" path="recordOfAcceptanceSmrNumber" id="recordOfAcceptanceSmrNumber" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="recordOfAcceptanceSmrNumber" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="recordOfAcceptanceSmrDate">Дата</label>
                <div class="col-md-7">
                    <sf:input type="text" placeholder="dd-MM-yyyy" path="recordOfAcceptanceSmrDate" id="recordOfAcceptanceSmrDate" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="recordOfAcceptanceSmrDate" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="sumRecordOfAcceptancePirWithNds">Сумма Акт сдачи-приемкиПИР; (с НДС)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="sumRecordOfAcceptancePirWithNds" id="sumRecordOfAcceptancePirWithNds" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="sumRecordOfAcceptancePirWithNds" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="sumRecordOfAcceptanceSmrWithNds">Сумма Акт сдачи-приемкиСМР; (с НДС)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="sumRecordOfAcceptanceSmrWithNds" id="sumRecordOfAcceptanceSmrWithNds" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="sumRecordOfAcceptanceSmrWithNds" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="notesEstimateAndContractDepartment">Примечания сметно-договорного отдела</label>
                <div class="col-md-7">
                    <sf:input type="text" path="notesEstimateAndContractDepartment" id="notesEstimateAndContractDepartment" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="notesEstimateAndContractDepartment" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="projectAgreed">Проект (согласован)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="projectAgreed" id="projectAgreed" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="projectAgreed" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="approvalProjectWithServices">Согласование проекта (со службами)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="approvalProjectWithServices" id="approvalProjectWithServices" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="approvalProjectWithServices" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="developer">Разработчик</label>
                <div class="col-md-7">
                    <sf:input type="text" path="developer" id="developer" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="developer" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="actualPerformanceOfWorkPercentageOfTotal">Фактическое выполнение работ ( в %, от общего объема )</label>
                <div class="col-md-7">
                    <sf:input type="text" path="actualPerformanceOfWorkPercentageOfTotal" id="actualPerformanceOfWorkPercentageOfTotal" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="actualPerformanceOfWorkPercentageOfTotal" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="producerOfWork">Производитель работ</label>
                <div class="col-md-7">
                    <sf:input type="text" path="producerOfWork" id="producerOfWork" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="producerOfWork" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="executiveDocumentationElectronic">Исполнительная документация (электронный вид)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="executiveDocumentationElectronic" id="executiveDocumentationElectronic" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="executiveDocumentationElectronic" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="executiveDocumentationWithSign">Исполнительная документация (подписанная)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="executiveDocumentationWithSign" id="executiveDocumentationWithSign" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="executiveDocumentationWithSign" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="developmentExecutiveDocuments">Разработчик Исполнительной документации</label>
                <div class="col-md-7">
                    <sf:input type="text" path="developmentExecutiveDocuments" id="developmentExecutiveDocuments" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="developmentExecutiveDocuments" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="invoicesCopiesOfInvoices">Счета (копии счетов)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="invoicesCopiesOfInvoices" id="invoicesCopiesOfInvoices" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="invoicesCopiesOfInvoices" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="costsOfTheWholeProject">Затраты (по объекту в целом)</label>
                <div class="col-md-7">
                    <sf:input type="text" path="costsOfTheWholeProject" id="costsOfTheWholeProject" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="costsOfTheWholeProject" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="handoverOfDocumentation">сдача документации:проекта,ИД,актов по форме КС-2, КС-3;№,дата письма передачи</label>
                <div class="col-md-7">
                    <sf:input type="text" path="handoverOfDocumentation" id="handoverOfDocumentation" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="handoverOfDocumentation" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="listOfDocumentsAfterEndJob">Список документов после завершения работ</label>
                <div class="col-md-7">
                    <sf:input type="text" path="listOfDocumentsAfterEndJob" id="listOfDocumentsAfterEndJob" class="form-control input-sm" />
                    <div class="has-error">
                        <sf:errors path="listOfDocumentsAfterEndJob" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-actions floatRight">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/orders/orders' />">Cancel</a>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/orders/orders' />">Cancel</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </sf:form>

</div>
</body>
</html>