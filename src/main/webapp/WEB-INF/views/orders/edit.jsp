<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit order page</title>
</head>
<body>
<h2>Create new order</h2>

<sf:form method="post" modelAttribute="orderDataModel" enctype="multipart/form-data">
    <fieldset>
        <table cellspacing="0">
            <tr>
                <th>Выберите файл</th>
                <td><input name="importFile" type="file"<%-- multiple--%>></td>
            </tr>

            <tr>
                <th>
                    Файлы которые уже прикреплены<br>
                    к этому документу
                </th>
                <td>
                    <h3>
                            <%--Это конструкция if...else--%>
                        <c:choose>
                            <c:when test="${orderDataModel.files.size() > 0}">
                                <c:forEach items="${orderDataModel.files}" var="file">
                                    <a href="/files/${file.fileName}" download>${file.fileName}</a><br>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <h3>Нет прикреплённых файлов</h3>
                            </c:otherwise>
                        </c:choose>
                    </h3>
                </td>
            </tr>

            <tr>
                <th>№ договора</th>
                <td>
                    <sf:input path="orderNumber" id="id_order_number"/>
                    <sf:errors path="orderNumber" cssClass="error"/>
                </td>
                <th>дата договора</th>
                <td>
                    <sf:input path="orderDate" placeholder="dd-MM-yyyy"/>
                    <br>
                    <sf:errors path="orderDate" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Наименование работ</th>
                <td>
                    <sf:input path="worksname" id="id_workname"/>
                    <sf:errors path="worksname" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Контрагент</th>
                <td>
                    <c:if test="${orderDataModel.contragentModel.firmName.length() > 0}">
                        ${orderDataModel.contragentModel.firmName = ""}
                        ${orderDataModel.contragentModel.firmAddress = ""}
                    </c:if>
                    <sf:input path="contragentModel.firmName"/> - Firm name
                    <sf:errors path="contragentModel.firmName" cssClass="error"/>
                    <br>
                    <sf:input path="contragentModel.firmAddress"/> - Firm address
                    <sf:errors path="contragentModel.firmAddress" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>или выберите фирму из списка
                    <br>
                    <sf:select path="contragentModel.id">
                        <sf:option value="-1" label="---Выберите фирму"/>
                        <sf:options items="${contragentsList}" itemValue="id" itemLabel="firmName"/>
                    </sf:select></td>
            </tr>

            <tr>
                <th>Сумма ПИР</th>
                <td><sf:input path="sumPIR" id="id_sumPIR"/></td>
                <sf:errors path="sumPIR" cssClass="error"/>
            </tr>
            <tr>
                <th>Сумма СМР</th>
                <td><sf:input path="sumSMR" id="id_sumSMR"/></td>
                <sf:errors path="sumSMR" cssClass="error"/>
            </tr>
            <tr>
                <th>Сроки выполнения работ<br>Начало:</th>
                <td><sf:input path="turnaroundTimeStart" id="id_turnaroundTimeStart" placeholder="dd-MM-yyyy"/><br>
                    <sf:errors path="turnaroundTimeStart" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Конец:</th>
                <td>
                    <sf:input path="turnaroundTimeEnd" id="id_turnaroundTimeEnd" placeholder="dd-MM-yyyy"/>
                    <br>
                    <sf:errors path="turnaroundTimeEnd" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Оплата (аванс)</th>
                <td><sf:input path="prepayment" id="id_prepayment"/></td>
                <sf:errors path="prepayment" cssClass="error"/>
            </tr>
            <tr>
                <th>Акт сдачи-приемки ПИР - номер:<br>
                    Акт сдачи-приемки ПИР - дата:
                </th>
                <td><sf:input path="recordOfAcceptancePirNumber" id="id_recordOfAcceptancePirNumber"
                              placeholder="номер"/><br>
                    <sf:errors path="recordOfAcceptancePirNumber" cssClass="error"/>
                    <sf:input path="recordOfAcceptancePirDate" id="id_recordOfAcceptancePirDate"
                              placeholder="dd-MM-yyyy"/></td>
                <sf:errors path="recordOfAcceptancePirDate" cssClass="error"/>
            </tr>
            <tr>
                <th>Акт сдачи-приемкиСМР - номер<br>
                    Акт сдачи-приемкиСМР - дата
                </th>
                <td><sf:input path="recordOfAcceptanceSmrNumber" id="id_recordOfAcceptanceSmrNumber"
                              placeholder="номер"/><br>
                    <sf:errors path="recordOfAcceptanceSmrNumber" cssClass="error"/>
                    <sf:input path="recordOfAcceptanceSmrDate" id="id_recordOfAcceptanceSmrDate"
                              placeholder="dd-MM-yyyy"/></td>
                <sf:errors path="recordOfAcceptanceSmrDate" cssClass="error"/>
            </tr>
            <tr>
                <th>Сумма Акт сдачи-приемкиПИР; (с НДС)</th>
                <td><sf:input path="sumRecordOfAcceptancePirWithNds" id="id_sumRecordOfAcceptancePirWithNds"/></td>
                <sf:errors path="sumRecordOfAcceptancePirWithNds" cssClass="error"/>
            </tr>
            <tr>
                <th>Сумма Акт сдачи-приемкиСМР; (с НДС)</th>
                <td><sf:input path="sumRecordOfAcceptanceSmrWithNds" id="id_sumRecordOfAcceptanceSmrWithNds"/></td>
                <sf:errors path="sumRecordOfAcceptanceSmrWithNds" cssClass="error"/>
            </tr>
            <tr>
                <th>Примечания сметно-договорного отдела</th>
                <td><sf:input path="notesEstimateAndContractDepartment"
                              id="id_notesEstimateAndContractDepartment"/></td>
                <sf:errors path="notesEstimateAndContractDepartment" cssClass="error"/>
            </tr>
            <tr>
                <th>Проект (согласован)</th>
                <td><sf:input path="projectAgreed" id="id_projectAgreed"/></td>
                <sf:errors path="projectAgreed" cssClass="error"/>
            </tr>
            <tr>
                <th>Согласование проекта (со службами)</th>
                <td><sf:input path="approvalProjectWithServices" id="id_approvalProjectWithServices"/>
                    <sf:errors path="approvalProjectWithServices" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Разработчик</th>
                <td><sf:input path="developer" id="id_developer"/>
                    <sf:errors path="developer" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Фактическое выполнение работ ( в %, от общего объема )</th>
                <td><sf:input path="actualPerformanceOfWorkPercentageOfTotal"
                              id="id_actualPerformanceOfWorkPercentageOfTotal"/>
                    <sf:errors path="actualPerformanceOfWorkPercentageOfTotal" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Производитель работ</th>
                <td><sf:input path="producerOfWork" id="id_producerOfWork"/>
                    <sf:errors path="producerOfWork" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Исполнительная документация (электронный вид)</th>
                <td><sf:input path="executiveDocumentationElectronic" id="id_executiveDocumentationElectronic"/>
                    <sf:errors path="executiveDocumentationElectronic" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Исполнительная документация (подписанная)</th>
                <td><sf:input path="executiveDocumentationWithSign" id="id_executiveDocumentationWithSign"/>
                    <sf:errors path="executiveDocumentationWithSign" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Разработчик Исполнительной документации</th>
                <td><sf:input path="developmentExecutiveDocuments" id="id_developmentExecutiveDocuments"/>
                    <sf:errors path="developmentExecutiveDocuments" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Счета (копии счетов)</th>
                <td><sf:input path="invoicesCopiesOfInvoices" id="id_invoicesCopiesOfInvoices"/>
                    <sf:errors path="invoicesCopiesOfInvoices" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Затраты (по объекту в целом)</th>
                <td><sf:input path="costsOfTheWholeProject" id="id_costsOfTheWholeProject"/>
                    <sf:errors path="costsOfTheWholeProject" cssClass="error"/></td>
            </tr>
            <tr>
                <th>сдача документации:проекта,ИД,актов по форме КС-2, КС-3;№,дата письма передачи
                </th>
                <td>
                    <sf:input path="handoverOfDocumentation" id="id_handoverOfDocumentation"/>
                    <sf:errors path="handoverOfDocumentation" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <th>Текущее состояние</th>
                <td><sf:input path="currentState" id="id_currentState"/>
                    <sf:errors path="currentState" cssClass="error"/></td>
            </tr>
            <tr>
                <th>Список документов после завершения работ</th>
                <td><sf:input path="listOfDocumentsAfterEndJob" id="id_currentState"/>
                    <sf:errors path="listOfDocumentsAfterEndJob" cssClass="error"/></td>
            </tr>

        </table>
        <input type="submit">
    </fieldset>
</sf:form>
</body>
</html>
