<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page isELIgnored="false" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>

<head>
    <title>Orders List</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>

<body>
<div class="generic-container">
    <%@include file="../authheader.jsp" %>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Список ордеров</span></div>
        <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
        <div class="well">
            <div class="btn btn-default">
                <a href="<c:url value='/orders?new' />">Создать новый ордер</a>
            </div>
            <sec:authorize access="hasRole('ADMIN')">
            <div class="btn btn-default">
                <a href="<c:url value='/newuser' />">Добавить нового пользователя</a>
            </div>
            </sec:authorize>

        </div>
        </sec:authorize>
        <table class="simple-little-table" cellspacing='0'>
            <%--<table class="bordered">--%>
            <thead>
            <tr>
                <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
                    <th width="100"></th>
                </sec:authorize>
                <sec:authorize access="hasRole('ADMIN')">
                    <th width="100"></th>
                </sec:authorize>
                <th data-toggle="tooltip" title="Номер ордера">Номер ордера</th>
                <th data-toggle="tooltip" title="Дата ордера">Дата ордера</th>
                <th data-toggle="tooltip" title="Список файлов">Список файлов</th>
                <th width="300" <%--data-toggle="tooltip" title="Наименование работ"--%>>Наименование работ</th>
                <th data-toggle="tooltip" title="Контрагент">Контрагент</th>
                <th data-toggle="tooltip" title="сумма ПИР">сумма ПИР</th>
                <th data-toggle="tooltip" title="сумма СМР">сумма СМР</th>
                <th data-toggle="tooltip" title="сроки выполнения работ">Сроки выполнения работ</th>
                <th data-toggle="tooltip" title="Текущее состояние">Текущее состояние</th>
                <th data-toggle="tooltip" title="Оплата(аванс)">Оплата(аванс)</th>
                <th data-toggle="tooltip" title="Акт сдачи-приемки ПИР(№, дата)">Акт сдачи-приемки ПИР</th>
                <th data-toggle="tooltip" title="Акт сдачи-приемкиСМР(№, дата)">Акт сдачи-приемкиСМР</th>
                <th data-toggle="tooltip" title="Сумма Акт сдачи-приемкиПИР (с НДС)">Сумма Акт сдачи-приемкиПИР</th>
                <th data-toggle="tooltip" title="Сумма Акт сдачи-приемкиСМР(с НДС)">Сумма Акт сдачи-приемкиСМР(с НДС)
                </th>
                <th data-toggle="tooltip" title="римечания сметно-договорного отдела">Примечания сметно-договорного
                    отдела
                </th>
                <th data-toggle="tooltip" title="Проект (согласован)">Проект (согласован)</th>
                <th data-toggle="tooltip" title="Согласование проекта(со службами)">Согласование проекта</th>
                <th data-toggle="tooltip" title="Разработчик">Разработчик</th>
                <th data-toggle="tooltip" title="Фактическое выполнение работ (в %, от общего объема)">Фактическое
                    выполнение работ
                </th>
                <th data-toggle="tooltip" title="Производитель работ">Производитель работ</th>
                <th data-toggle="tooltip" title="Список файлов исполниьельной документации">Файлы исполнительной
                    документации
                </th>
                <th data-toggle="tooltip" title="Исполнительная документация (подписанная)">Исполнительная документация
                    (подписанная)
                </th>
                <th data-toggle="tooltip" title="Разработчик Исполнительной документации">Разработчик Исполнительной
                    документации
                </th>
                <th data-toggle="tooltip" title="Счета (копии счетов)">Счета (копии счетов)</th>
                <th data-toggle="tooltip" title="Затраты (по объекту в целом)">Затраты (по объекту в целом)</th>
                <th data-toggle="tooltip"
                    title="сдача документации: проекта, ИД, актов по форме КС-2, КС-3; №, дата письма передачи">Сдача
                    документации
                </th>
                <th data-toggle="tooltip" title="Список документов после завершения работ">Список документов после
                    завершения работ
                </th>
                <%--<th>Firstname</th>--%>
                <%--<th>Lastname</th>--%>
                <%--<th>Email</th>--%>
                <%--<th>SSO ID</th>--%>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orders}" var="order">
                <tr>
                    <sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
                        <td><a href="<c:url value='/orders/${order.id}?edit' />" class="btn btn-success custom-width">edit</a>
                        </td>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ADMIN')">
                        <td><a href="<c:url value='/orders/${order.id}?delete' />" class="btn btn-danger custom-width">delete</a>
                        </td>
                    </sec:authorize>
                        <%--<td><a href="/orders/${order.id}?edit">edit</a></td>--%>
                        <%--<td><a href="/orders/${order.id}?delete">delete</a></td>--%>
                    <td>${order.orderNumber}</td>
                    <td>
                        <fmt:formatDate value="${order.orderDate}" pattern="dd-MM-yyyy"/>
                    </td>

                    <td>
                        <c:choose><%--Это конструкция if...else--%>
                            <c:when test="${order.files.size() > 0}">
                                <c:forEach items="${order.files}" var="file">
                                    <%--<a href="/files/${file.fileName}" download>${file.fileName}</a><br>--%>
                                    <%--<a href="files/${file.fileName}">${file.fileName}</a><br>--%>
                                    <a href="<c:url value="/files/${file.fileName}" />">${file.fileName}</a><br>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                Нет прикреплённых файлов
                            </c:otherwise>
                        </c:choose>
                    </td>

                    <td width="300">${order.worksname}</td>
                    <td>${order.contragentModel.firmName}</td>
                    <td>${order.sumPIR} руб.</td>
                    <td>${order.sumSMR} руб.</td>
                    <td>
                        <fmt:formatDate value="${order.turnaroundTimeStart}" pattern="dd-MM-yyyy"/>
                        <br/>
                        <fmt:formatDate value="${order.turnaroundTimeEnd}" pattern="dd-MM-yyyy"/>
                    </td>
                    <td>${order.currentState}</td>

                    <td>${order.prepayment}</td>
                    <td>${order.recordOfAcceptancePirNumber}
                        <br/>
                        <fmt:formatDate value="${order.recordOfAcceptancePirDate}" pattern="dd-MM-yyyy"/>
                    </td>
                    <td>${order.recordOfAcceptanceSmrNumber}
                        <br/>
                        <fmt:formatDate value="${order.recordOfAcceptanceSmrDate}" pattern="dd-MM-yyyy"/>
                    </td>
                    <td>${order.sumRecordOfAcceptancePirWithNds} руб.</td>
                    <td>${order.sumRecordOfAcceptanceSmrWithNds} руб.</td>
                    <td>${order.notesEstimateAndContractDepartment}</td>
                    <td>${order.projectAgreed}</td>
                    <td>${order.approvalProjectWithServices}</td>
                    <td>${order.developer}</td>
                    <td>${order.actualPerformanceOfWorkPercentageOfTotal}</td>
                    <td>${order.producerOfWork}</td>
                    <td>${order.executiveDocumentationElectronic}</td>
                    <td>${order.executiveDocumentationWithSign}</td>
                    <td>${order.developmentExecutiveDocuments}</td>
                    <td>${order.invoicesCopiesOfInvoices}</td>
                    <td>${order.costsOfTheWholeProject}</td>
                    <td>${order.handoverOfDocumentation}</td>
                    <td>${order.listOfDocumentsAfterEndJob}</td>
                        <%--<td>${order.activeOrNot}</td>--%>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <sec:authorize access="hasRole('ADMIN')">
        <div class="well">
            <a href="<c:url value='/orders?new' />">Add New Order</a>
        </div>
    </sec:authorize>
</div>
</body>
</html>
