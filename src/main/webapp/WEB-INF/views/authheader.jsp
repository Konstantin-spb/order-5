<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication var="user" property="principal" />

<div class="authbar">
		<span>Dear <strong>${loggedinuser}</strong>, Добро пожаловать ${user.username}.</span> <span ><a href="<c:url value="/logout" />">Logout</a></span>
	</div>
	<script>
		// после загрузки страницы
		$(function () {
			// инициализировать все элементы на страницы, имеющих атрибут data-toggle="tooltip", как компоненты tooltip
			$('[data-toggle="tooltip"]').tooltip()
		})
	</script>
